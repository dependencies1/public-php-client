<?php

namespace Flymail;

class Flymail
{

    public static $secret_key = '';
    public static $log_dir = '/var/log/';

    /**
     * Set the secret key
     */
    public static function setSecretKey($__secret_key)
    {
        self::$secret_key = $__secret_key;
    }

    /**
     * Set the log dir
     */
    public static function setLogDir($__log_dir)
    {
        self::$log_dir = $__log_dir;
    }


    /**
     * Sends an email using Flymail API
     * This is the dispatcher function
     * (there are 2 ways of sending emails)
     */
    public static function send()
    {
        $numargs = func_num_args();
        $__arg[0] = func_get_arg(0);

        if ( $numargs >= 3 )
        {
            return self::sendSimple($__arg[0], func_get_arg(1), func_get_arg(2), func_get_arg(3));
        }
        elseif ( ($numargs==1) && (is_array($__arg[0])) )
        {
            return self::sendFull($__arg[0]);
        }
        else
        {
            // this call is not valid...
            return false;
        }
    }


    /**
     * Sends an email using Flymail API -- Full version
     */
    public static function sendFull($__arg)
    {
        // Validation
        $__to   = $__arg['to'];
        $__template = $__arg['template'];

        // Build array
        $http_body = [];
        $http_body['to'] = $__arg['to'];
        $http_body['vars'] = $__arg['vars'];
        $http_body['lang'] = $__arg['lang'];

        if ( isset($__arg['from']) && is_array($__arg['from']) )
        {
            $http_body['from'] = $__arg['from'];
        }

        return self::callHttpApi($__template, $http_body);
    }


    /**
     * Sends an email using Flymail API -- Simple version
     */
    public static function sendSimple($__template, $__to=null, $__vars=null, $__lang=null)
    {
        $http_body = array(
            'to'   => $__to,
            'vars' => $__vars,
            'lang' => $__lang
        );
        return self::callHttpApi($__template, $http_body);
    }


    /**
     * Sends an email using Flymail API -- Full version
     */
    public static function callHttpApi($__template, $http_body)
    {
        $ch = curl_init();

        // geosend.flymail.io
        curl_setopt($ch, CURLOPT_URL, "http://www.flymail.io/p/send/{$__template}");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "X-Secret-Key: " . self::$secret_key,
            "Content-Type: application/json",
        ));

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($http_body));

        $verbose = fopen('php://temp', 'w+');
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_STDERR, $verbose);
        $raw_response = curl_exec($ch);

        rewind($verbose);
        $verbose_log = stream_get_contents($verbose);
        curl_close($ch);

        // Write in logs
        self::writeLogs($verbose_log, $raw_response);

        $api_response = json_decode($raw_response, true);

        if(!$api_response)
        {
            return false;
        }

        // Yeyyyy!
        return $api_response;
    }


    /**
     * Write in logs
     */
    public static function writeLogs($verbose_log, $raw_response)
    {
        if ( strlen(self::$log_dir) > 0 )
        {
            $log_path = rtrim(self::$log_dir, '/').'/flymail.log';

            file_put_contents($log_path, "\n\n===\n".date('c')."\n===\n" . $verbose_log, FILE_APPEND);
            file_put_contents($log_path, "\n\n" . $raw_response, FILE_APPEND);
        }
    }


}
